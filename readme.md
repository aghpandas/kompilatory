# Installation

## Install Docker and run commands

```
docker-compose up --build
```

Interpreter will be made in python ( no docker for now ).

Container have access to shared folder where result should be stored.

## Install virtualenv (optional)

```
pip3 install virtualenv
virtualenv venv --python=python3
source venv/bin/activate
```

## Install dependencies for python3

```
pip3 install -r requirements.txt
```

# Develop

## Lunch bash in parser container ( R installed and antlr)

```
docker-compose exec parser bash
```

## Generate parser and listner based on gramma

Lunch bash in parser container and type:

```
antlr4 -Dlanguage=Python3 <Folder>/<File>.g4 -no-listener -visitor
# example: antlr4 -Dlanguage=Python3 /app/shared/R/R.g4 -no-listener -visitor
```

or go to shared/R and run

```
./generate.sh
```

# Use

## Pass file as an input

```
python3 shared/R/__init__.py shared/R/test.R
```

## Enter interactive console

```
python3 shared/R/__init__.py
```
