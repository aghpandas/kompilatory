import math
import re
import sys

import numpy as np

# from RListener import RListener
from FunctionVisitor import FunctionVisitor
from RParser import RParser
# from shared.R.RListener import RListener
from RVisitor import RVisitor


def frange(start, stop=None, step=None):
    listarr = []
    if stop is None:
        stop = start + 0.0
        start = 0.0
    if step is None:
        step = 1.0
    while True:
        if step > 0 and start > stop:
            break
        elif step < 0 and start < stop:
            break
        listarr.append(start)
        start = start + step
    return listarr


global_variables = {}


class BaseRVisitor(RVisitor):

    def __init__(self) -> None:
        self.functionVisitor = FunctionVisitor(self)
        super().__init__()

    def visitProg(self, ctx: RParser.ProgContext):
        for child in ctx.getChildren():
            result = self.visit(child)
            if isinstance(child, RParser.CompoundContext):
                continue
            if isinstance(child, RParser.AssignContext):
                continue
            if result is not None:
                print(result)

        return 0

    def visitParentheses(self, ctx: RParser.ParenthesesContext):
        return self.visit(ctx.expr())

    def visitAssign(self, ctx: RParser.AssignContext):
        # TODO: support assign of assignment, e.g. a = b = 1
        # TODO: add assign to character like variables
        if ctx.expr(0).ID() is None and ctx.expr(1).ID() is None:
            raise SyntaxError('nothing to assign')

        left_assign = {
            '<-': True,
            '=': True,
            '<<-': True,
            '->': False,
            '->>': False
        }[ctx.op.text]

        if left_assign:
            left = ctx.expr(0).ID()
            if left is None:
                raise SyntaxError("Incorrect left side of assignment")
            value = self.visit(ctx.expr(1))
            global_variables.update({left.getText(): value})
            return value

        right = ctx.expr(0).ID()
        if right is None:
            raise SyntaxError("Incorrect right side of assignment")
        value = self.visit(ctx.expr(0))
        global_variables.update({right.getText(): value})
        return value

    def visitVector(self, ctx: RParser.VectorContext):
        left = self.visit(ctx.expr(0))
        right = self.visit(ctx.expr(1))
        step = 1
        stepDirection = 1

        if type(left) is complex:
            left = 0

        if left > right:
            stepDirection = -1

        if type(right) is complex:
            step = 0 + 1j

        return np.array(frange(left, right, step * stepDirection))

    def visitAddSub(self, ctx: RParser.AddSubContext):
        left = self.visit(ctx.expr(0))
        right = self.visit(ctx.expr(1))

        if ctx.op.type == RParser.SUB:
            result = left - right
        if ctx.op.type == RParser.ADD:
            result = np.sum([left, right])
        return result

    def visitMulDiv(self, ctx: RParser.MulDivContext):
        left = self.visit(ctx.expr(0))
        right = self.visit(ctx.expr(1))

        if ctx.op.type == RParser.MUL:
            result = left * right
        if ctx.op.type == RParser.DIV:
            result = left / right
        return result

    def visitSignChange(self, ctx: RParser.SignChangeContext):
        expr = self.visit(ctx.expr())
        return -1 * expr

    def visitAtom(self, ctx: RParser.AtomContext):
        if ctx.INT() is not None:
            text = re.sub('[Ll]', '', ctx.getText())
            return int(text)
        elif ctx.FLOAT() is not None:
            text = re.sub('[Ll]', '', ctx.getText())
            return float(text)
        elif ctx.COMPLEX() is not None:
            return complex(ctx.getText().replace('i', 'j'))
        elif ctx.HEX() is not None:
            text = re.sub('[Ll]', '', ctx.getText())
            return int(text, 0)
        elif ctx.ID() is not None:
            variable = global_variables.get(ctx.ID().getText())
            if variable is None:
                raise Exception('No variable found')
            return variable
        elif ctx.CHARACTER() is not None:
            return re.sub('[\\"\']', '', ctx.getText())
        return ctx.getText()

    def visitPower(self, ctx: RParser.PowerContext):
        left = self.visit(ctx.expr(0))
        right = self.visit(ctx.expr(1))

        return np.power(left, right)

    def visitCompare(self, ctx: RParser.CompareContext):
        left = self.visit(ctx.expr(0))
        right = self.visit(ctx.expr(1))

        return {
            '>': lambda x, y: x > y,
            '>=': lambda x, y: x >= y,
            '<': lambda x, y: x < y,
            '<=': lambda x, y: x <= y,
            '==': lambda x, y: x == y,
            '!=': lambda x, y: x != y
        }[ctx.op.text](left, right)

    def visitUnary(self, ctx: RParser.UnaryContext):
        return {
            '!': lambda x: np.logical_not(x)
        }[ctx.op.text](self.visit(ctx.expr()))

    def visitLogicAnd(self, ctx: RParser.LogicAndContext):
        left = self.visit(ctx.expr(0))
        right = self.visit(ctx.expr(1))

        return left and right

    def visitLogicOr(self, ctx: RParser.LogicAndContext):
        left = self.visit(ctx.expr(0))
        right = self.visit(ctx.expr(1))

        return left or right

    def visitSeq(self, ctx: RParser.SeqContext):
        args = self.visit(ctx.sublist())
        args_length = len(args)
        step = 1
        try:
            if not np.isfinite(args[0]):
                raise SyntaxError("From must be finite")
        except:
            raise SyntaxError("From must be finite")
        if args_length > 1 and not np.isfinite(args[1]):
            raise SyntaxError("To must be finite")

        left = args[0]
        right = args[1]

        if left > right:
            step = -1
        if args_length == 3:
            step = args[2]

        return np.array(frange(left, right, step))

    def visitSublist(self, ctx: RParser.SublistContext):
        sublist = []
        for child in ctx.getChildren():
            if not isinstance(child, RParser.SubContext):
                continue
            result = self.visit(child)
            if result is not None:
                sublist.append(result)
        return np.array(sublist)

    def visitSub(self, ctx: RParser.SubContext):
        child_count = ctx.getChildCount()
        is_assign = ctx.getChild(1).getText(
        ) is '=' if child_count == 2 else False

        if child_count == 2 and not is_assign:
            raise SyntaxError(" no = mark")
        if ctx.expr() is not None:
            return self.visit(ctx.expr())

        if ctx.CHARACTER() is not None:
            if child_count is 3:
                return {ctx.getChild(0).getText() + '=': self.visit(ctx.expr())}

        if ctx.ID() is not None:
            if child_count is 3:
                return {ctx.getChild(0).getText() + '=': self.visit(ctx.expr())}

        return ctx.getChild(0).getText() + '='

    def visitFunctionCall(self, ctx: RParser.FunctionCallContext):
        return self.functionVisitor.visit(ctx.expr().getText(), ctx)

    def visitCompound(self, ctx: RParser.CompoundContext):
        if isinstance(ctx.getChild(1), RParser.ExprlistContext):
            return self.visit(ctx.exprlist())

        # TODO: Shout return NULL not the token of NULL
        return RParser.NULL

    def visitExprlist(self, ctx: RParser.ExprlistContext):
        result = None

        for child in ctx.getChildren():
            result = self.visit(child)

        return result
