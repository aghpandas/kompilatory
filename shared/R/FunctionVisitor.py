import RParser
from NodeVisitor import NodeVisitor
import numpy as np

if __name__ is not None and "." in __name__:
    from .RParser import RParser
else:
    from RParser import RParser


class FunctionVisitor(NodeVisitor):
    def visit_c(self, ctx: RParser.FunctionCallContext):
        # ToDo: fix type conversion
        sublist = self.baseVisitor.visit(ctx.sublist())
        result = []
        for child in sublist:
            if isinstance(child, np.ndarray):
                result = np.concatenate([result, child])
            else:
                result = np.concatenate([result, [child]])
        return result

    def visit_max(self, ctx: RParser.FunctionCallContext):
        sublist = self.baseVisitor.visit(ctx.sublist())
        args_length = len(sublist)
        if args_length > 1:
            return np.max(sublist)
        return np.max(sublist[0])

    def visit_min(self, ctx: RParser.FunctionCallContext):
        sublist = self.baseVisitor.visit(ctx.sublist())
        args_length = len(sublist)
        if args_length > 1:
            return np.min(sublist)
        return np.min(sublist[0])

    def visit_sum(self, ctx: RParser.FunctionCallContext):
        sublist = self.baseVisitor.visit(ctx.sublist())
        args_length = len(sublist)
        if args_length > 1:
            return np.sum(np.concatenate(sublist))
        return np.sum(sublist[0])

    def visit_matrix(self, ctx: RParser.FunctionCallContext):
        sublist = self.baseVisitor.visit(ctx.sublist())
        matrix = []
        args_length = len(sublist)
        basicArr = sublist[0]
        arr_length = len(basicArr)
        nRows = sublist[1]
        nCols = sublist[2]
        byRows = args_length == 4 and sublist[3]

        for row in range(nRows):
            matrix_row = []
            for col in range(nCols):
                if byRows:
                    matrix_row.append(basicArr[(row * nCols + col) % arr_length])
                else:
                    matrix_row.append(basicArr[(col * nRows + row) % arr_length])

            matrix.append(matrix_row)
        return np.array(matrix)

    def visit_t(self, ctx: RParser.FunctionCallContext):
        sublist = self.baseVisitor.visit(ctx.sublist())
        matrix = sublist[0]
        return np.transpose(matrix)
