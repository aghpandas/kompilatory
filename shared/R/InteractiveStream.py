import codecs
import sys

from antlr4.InputStream import InputStream


class InteractiveStream(InputStream):
    def __init__(self, line=b'', encoding='ascii', errors='strict'):
        data = codecs.decode(line, encoding, errors)
        super().__init__(data)
