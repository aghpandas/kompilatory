class NodeVisitor(object):
    def __init__(self, base_visitor):
        self.baseVisitor = base_visitor

    def visit(self, node, ctx):
        method_name = 'visit_' + node
        try:
            visitor = getattr(self, method_name)
            return visitor(ctx)
        except AttributeError:
            self.generic_visit(node)

    def generic_visit(self, node):
        raise Exception('No {} method'.format(node))
