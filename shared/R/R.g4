grammar R;

prog: ( expr (';' | NL)* | NL)* EOF;

expr:
//        expr '[[' sublist ']' ']'  // '[[' follows R-test's yacc grammar
//    |   expr '[' sublist ']'
//    |   expr ('::'|':::') expr
//    |   expr ('$'|'@') expr
//    |
        '(' expr ')'                                #Parentheses
    |   <assoc=right> expr op=('^'|'**') expr       #Power
    |   ('-') expr                                  #SignChange
    |   expr ':' expr                               #Vector
//    |   expr USER_OP expr // anything wrappedin %: '%' .* '%'
    |   expr op=(MUL|DIV) expr                         #MulDiv
    |   expr op=(ADD|SUB) expr                         #AddSub
    |   expr op=('>'|'>='|'<'|'<='|'=='|'!=') expr     #Compare
    |   op='!' expr                                    #Unary
    |   expr op=('&'|'&&') expr                        #LogicAnd
    |   expr op=('|'|'||') expr                        #LogicOr
    |   '~' expr                                    #Unary
//    |   expr '~' expr
    |   expr op=('<-'|'<<-'|'='|'->'|'->>') expr    #Assign
//    |   'function' '(' formlist? ')' expr // define function
    |   expr '(' sublist ')'                            #FunctionCall
    |   'seq(' sublist ')'                             #Seq
    |   '{' exprlist '}' 															#Compound // compound statement
//    |   'if' '(' expr ')' expr
//    |   'if' '(' expr ')' expr 'else' expr
//    |   'for' '(' ID 'in' expr ')' expr
//    |   'while' '(' expr ')' expr
//    |   'repeat' expr
//    |   '?' expr // get help on expr, usually CHARACTER or ID
//    |   'next'
//    |   'break'
    |   ID              #Atom
    |   CHARACTER       #Atom
    |   HEX             #Atom
    |   INT             #Atom
    |   FLOAT           #Atom
    |   COMPLEX         #Atom
    |   NULL            #Atom
    |   NA              #Atom
    |   Inf             #Atom
    |   NaN             #Atom
    |   TRUE            #Atom
    |   FALSE           #Atom
    ;

exprlist
    :   expr ((';'|NL) expr?)*
    |
    ;

/*formlist : form (',' form)* ;

form:   ID
    |   ID '=' expr
    |   '...'
    |   '.'
    ;*/

sublist : sub (',' sub)* ;

sub :   expr
    |   ID '='
    |   ID '=' expr
    |   CHARACTER '='
    |   CHARACTER '=' expr
    |   'NULL' '='
    |   'NULL' '=' expr
    |   '...'
    |   '.'
    |
    ;

HEX :   '0' ('x'|'X') HEXDIGIT+ [Ll]? ;

INT :   DIGIT+ [Ll]? ;

fragment
HEXDIGIT : ('0'..'9'|'a'..'f'|'A'..'F') ;

FLOAT:  DIGIT+ '.' DIGIT* EXP? [Ll]?
    |   DIGIT+ EXP? [Ll]?
    |   '.' DIGIT+ EXP? [Ll]?
    ;
fragment
DIGIT:  '0'..'9' ;
fragment
EXP :   ('E' | 'e') ('+' | '-')? INT ;

MUL: '*';
DIV: '/';
ADD: '+';
SUB: '-';

NA: 'NA';
Inf: 'Inf';
NaN: 'NaN';
NULL: 'NULL';
TRUE: 'TRUE';
FALSE: 'FALSE';

COMPLEX: INT 'i' | FLOAT 'i';

CHARACTER:
	'"' (ESC | ~[\\"])*? '"'
	| '\'' ( ESC | ~[\\'])*? '\''
	| '`' ( ESC | ~[\\'])*? '`';

fragment ESC:
	'\\' [abtnfrv"'\\]
	| UNICODE_ESCAPE
	| HEX_ESCAPE
	| OCTAL_ESCAPE;

fragment UNICODE_ESCAPE:
	'\\' 'u' HEXDIGIT HEXDIGIT HEXDIGIT HEXDIGIT
	| '\\' 'u' '{' HEXDIGIT HEXDIGIT HEXDIGIT HEXDIGIT '}';

fragment OCTAL_ESCAPE:
	'\\' [0-3] [0-7] [0-7]
	| '\\' [0-7] [0-7]
	| '\\' [0-7];

fragment HEX_ESCAPE: '\\' HEXDIGIT HEXDIGIT?;

ID:
	'.' (LETTER | '_' | '.') (LETTER | DIGIT | '_' | '.')*
	| LETTER (LETTER | DIGIT | '_' | '.')*;

fragment LETTER: [a-zA-Z];

USER_OP: '%' .*? '%';

COMMENT: '#' .*? '\r'? '\n' -> type(NL);

// Match both UNIX and Windows newlines
NL: '\r'? '\n' -> skip;

WS: [ \t\u000C]+ -> skip;
