# Generated from R.g4 by ANTLR 4.7.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\62")
        buf.write("\u0089\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\3\2\3\2")
        buf.write("\7\2\17\n\2\f\2\16\2\22\13\2\3\2\7\2\25\n\2\f\2\16\2\30")
        buf.write("\13\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3;\n\3\3\3\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\7\3Z")
        buf.write("\n\3\f\3\16\3]\13\3\3\4\3\4\3\4\5\4b\n\4\7\4d\n\4\f\4")
        buf.write("\16\4g\13\4\3\4\5\4j\n\4\3\5\3\5\3\5\7\5o\n\5\f\5\16\5")
        buf.write("r\13\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6")
        buf.write("\3\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6\u0087\n\6\3\6\2\3\4\7")
        buf.write("\2\4\6\b\n\2\n\4\2\3\3\61\61\3\2\6\7\3\2#$\3\2%&\3\2\t")
        buf.write("\16\3\2\20\21\3\2\22\23\3\2\25\31\2\u00ad\2\26\3\2\2\2")
        buf.write("\4:\3\2\2\2\6i\3\2\2\2\bk\3\2\2\2\n\u0086\3\2\2\2\f\20")
        buf.write("\5\4\3\2\r\17\t\2\2\2\16\r\3\2\2\2\17\22\3\2\2\2\20\16")
        buf.write("\3\2\2\2\20\21\3\2\2\2\21\25\3\2\2\2\22\20\3\2\2\2\23")
        buf.write("\25\7\61\2\2\24\f\3\2\2\2\24\23\3\2\2\2\25\30\3\2\2\2")
        buf.write("\26\24\3\2\2\2\26\27\3\2\2\2\27\31\3\2\2\2\30\26\3\2\2")
        buf.write("\2\31\32\7\2\2\3\32\3\3\2\2\2\33\34\b\3\1\2\34\35\7\4")
        buf.write("\2\2\35\36\5\4\3\2\36\37\7\5\2\2\37;\3\2\2\2 !\7&\2\2")
        buf.write("!;\5\4\3\33\"#\7\17\2\2#;\5\4\3\26$%\7\24\2\2%;\5\4\3")
        buf.write("\23&\'\7\32\2\2\'(\5\b\5\2()\7\5\2\2);\3\2\2\2*+\7\33")
        buf.write("\2\2+,\5\6\4\2,-\7\34\2\2-;\3\2\2\2.;\7/\2\2/;\7.\2\2")
        buf.write("\60;\7 \2\2\61;\7!\2\2\62;\7\"\2\2\63;\7-\2\2\64;\7*\2")
        buf.write("\2\65;\7\'\2\2\66;\7(\2\2\67;\7)\2\28;\7+\2\29;\7,\2\2")
        buf.write(":\33\3\2\2\2: \3\2\2\2:\"\3\2\2\2:$\3\2\2\2:&\3\2\2\2")
        buf.write(":*\3\2\2\2:.\3\2\2\2:/\3\2\2\2:\60\3\2\2\2:\61\3\2\2\2")
        buf.write(":\62\3\2\2\2:\63\3\2\2\2:\64\3\2\2\2:\65\3\2\2\2:\66\3")
        buf.write("\2\2\2:\67\3\2\2\2:8\3\2\2\2:9\3\2\2\2;[\3\2\2\2<=\f\34")
        buf.write("\2\2=>\t\3\2\2>Z\5\4\3\34?@\f\32\2\2@A\7\b\2\2AZ\5\4\3")
        buf.write("\33BC\f\31\2\2CD\t\4\2\2DZ\5\4\3\32EF\f\30\2\2FG\t\5\2")
        buf.write("\2GZ\5\4\3\31HI\f\27\2\2IJ\t\6\2\2JZ\5\4\3\30KL\f\25\2")
        buf.write("\2LM\t\7\2\2MZ\5\4\3\26NO\f\24\2\2OP\t\b\2\2PZ\5\4\3\25")
        buf.write("QR\f\22\2\2RS\t\t\2\2SZ\5\4\3\23TU\f\21\2\2UV\7\4\2\2")
        buf.write("VW\5\b\5\2WX\7\5\2\2XZ\3\2\2\2Y<\3\2\2\2Y?\3\2\2\2YB\3")
        buf.write("\2\2\2YE\3\2\2\2YH\3\2\2\2YK\3\2\2\2YN\3\2\2\2YQ\3\2\2")
        buf.write("\2YT\3\2\2\2Z]\3\2\2\2[Y\3\2\2\2[\\\3\2\2\2\\\5\3\2\2")
        buf.write("\2][\3\2\2\2^e\5\4\3\2_a\t\2\2\2`b\5\4\3\2a`\3\2\2\2a")
        buf.write("b\3\2\2\2bd\3\2\2\2c_\3\2\2\2dg\3\2\2\2ec\3\2\2\2ef\3")
        buf.write("\2\2\2fj\3\2\2\2ge\3\2\2\2hj\3\2\2\2i^\3\2\2\2ih\3\2\2")
        buf.write("\2j\7\3\2\2\2kp\5\n\6\2lm\7\35\2\2mo\5\n\6\2nl\3\2\2\2")
        buf.write("or\3\2\2\2pn\3\2\2\2pq\3\2\2\2q\t\3\2\2\2rp\3\2\2\2s\u0087")
        buf.write("\5\4\3\2tu\7/\2\2u\u0087\7\27\2\2vw\7/\2\2wx\7\27\2\2")
        buf.write("x\u0087\5\4\3\2yz\7.\2\2z\u0087\7\27\2\2{|\7.\2\2|}\7")
        buf.write("\27\2\2}\u0087\5\4\3\2~\177\7*\2\2\177\u0087\7\27\2\2")
        buf.write("\u0080\u0081\7*\2\2\u0081\u0082\7\27\2\2\u0082\u0087\5")
        buf.write("\4\3\2\u0083\u0087\7\36\2\2\u0084\u0087\7\37\2\2\u0085")
        buf.write("\u0087\3\2\2\2\u0086s\3\2\2\2\u0086t\3\2\2\2\u0086v\3")
        buf.write("\2\2\2\u0086y\3\2\2\2\u0086{\3\2\2\2\u0086~\3\2\2\2\u0086")
        buf.write("\u0080\3\2\2\2\u0086\u0083\3\2\2\2\u0086\u0084\3\2\2\2")
        buf.write("\u0086\u0085\3\2\2\2\u0087\13\3\2\2\2\r\20\24\26:Y[ae")
        buf.write("ip\u0086")
        return buf.getvalue()


class RParser ( Parser ):

    grammarFileName = "R.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "';'", "'('", "')'", "'^'", "'**'", "':'", 
                     "'>'", "'>='", "'<'", "'<='", "'=='", "'!='", "'!'", 
                     "'&'", "'&&'", "'|'", "'||'", "'~'", "'<-'", "'<<-'", 
                     "'='", "'->'", "'->>'", "'seq('", "'{'", "'}'", "','", 
                     "'...'", "'.'", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "'*'", "'/'", "'+'", "'-'", "'NA'", "'Inf'", "'NaN'", 
                     "'NULL'", "'TRUE'", "'FALSE'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "HEX", "INT", "FLOAT", "MUL", 
                      "DIV", "ADD", "SUB", "NA", "Inf", "NaN", "NULL", "TRUE", 
                      "FALSE", "COMPLEX", "CHARACTER", "ID", "USER_OP", 
                      "NL", "WS" ]

    RULE_prog = 0
    RULE_expr = 1
    RULE_exprlist = 2
    RULE_sublist = 3
    RULE_sub = 4

    ruleNames =  [ "prog", "expr", "exprlist", "sublist", "sub" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    T__13=14
    T__14=15
    T__15=16
    T__16=17
    T__17=18
    T__18=19
    T__19=20
    T__20=21
    T__21=22
    T__22=23
    T__23=24
    T__24=25
    T__25=26
    T__26=27
    T__27=28
    T__28=29
    HEX=30
    INT=31
    FLOAT=32
    MUL=33
    DIV=34
    ADD=35
    SUB=36
    NA=37
    Inf=38
    NaN=39
    NULL=40
    TRUE=41
    FALSE=42
    COMPLEX=43
    CHARACTER=44
    ID=45
    USER_OP=46
    NL=47
    WS=48

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class ProgContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EOF(self):
            return self.getToken(RParser.EOF, 0)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RParser.ExprContext)
            else:
                return self.getTypedRuleContext(RParser.ExprContext,i)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(RParser.NL)
            else:
                return self.getToken(RParser.NL, i)

        def getRuleIndex(self):
            return RParser.RULE_prog

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProg" ):
                return visitor.visitProg(self)
            else:
                return visitor.visitChildren(self)




    def prog(self):

        localctx = RParser.ProgContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_prog)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 20
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << RParser.T__1) | (1 << RParser.T__12) | (1 << RParser.T__17) | (1 << RParser.T__23) | (1 << RParser.T__24) | (1 << RParser.HEX) | (1 << RParser.INT) | (1 << RParser.FLOAT) | (1 << RParser.SUB) | (1 << RParser.NA) | (1 << RParser.Inf) | (1 << RParser.NaN) | (1 << RParser.NULL) | (1 << RParser.TRUE) | (1 << RParser.FALSE) | (1 << RParser.COMPLEX) | (1 << RParser.CHARACTER) | (1 << RParser.ID) | (1 << RParser.NL))) != 0):
                self.state = 18
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [RParser.T__1, RParser.T__12, RParser.T__17, RParser.T__23, RParser.T__24, RParser.HEX, RParser.INT, RParser.FLOAT, RParser.SUB, RParser.NA, RParser.Inf, RParser.NaN, RParser.NULL, RParser.TRUE, RParser.FALSE, RParser.COMPLEX, RParser.CHARACTER, RParser.ID]:
                    self.state = 10
                    self.expr(0)
                    self.state = 14
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,0,self._ctx)
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt==1:
                            self.state = 11
                            _la = self._input.LA(1)
                            if not(_la==RParser.T__0 or _la==RParser.NL):
                                self._errHandler.recoverInline(self)
                            else:
                                self._errHandler.reportMatch(self)
                                self.consume() 
                        self.state = 16
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,0,self._ctx)

                    pass
                elif token in [RParser.NL]:
                    self.state = 17
                    self.match(RParser.NL)
                    pass
                else:
                    raise NoViableAltException(self)

                self.state = 22
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 23
            self.match(RParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return RParser.RULE_expr

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class LogicOrContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RParser.ExprContext
            super().__init__(parser)
            self.op = None # Token
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RParser.ExprContext)
            else:
                return self.getTypedRuleContext(RParser.ExprContext,i)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLogicOr" ):
                return visitor.visitLogicOr(self)
            else:
                return visitor.visitChildren(self)


    class MulDivContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RParser.ExprContext
            super().__init__(parser)
            self.op = None # Token
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RParser.ExprContext)
            else:
                return self.getTypedRuleContext(RParser.ExprContext,i)

        def MUL(self):
            return self.getToken(RParser.MUL, 0)
        def DIV(self):
            return self.getToken(RParser.DIV, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMulDiv" ):
                return visitor.visitMulDiv(self)
            else:
                return visitor.visitChildren(self)


    class AddSubContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RParser.ExprContext
            super().__init__(parser)
            self.op = None # Token
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RParser.ExprContext)
            else:
                return self.getTypedRuleContext(RParser.ExprContext,i)

        def ADD(self):
            return self.getToken(RParser.ADD, 0)
        def SUB(self):
            return self.getToken(RParser.SUB, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAddSub" ):
                return visitor.visitAddSub(self)
            else:
                return visitor.visitChildren(self)


    class CompoundContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def exprlist(self):
            return self.getTypedRuleContext(RParser.ExprlistContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCompound" ):
                return visitor.visitCompound(self)
            else:
                return visitor.visitChildren(self)


    class SignChangeContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(RParser.ExprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSignChange" ):
                return visitor.visitSignChange(self)
            else:
                return visitor.visitChildren(self)


    class UnaryContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RParser.ExprContext
            super().__init__(parser)
            self.op = None # Token
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(RParser.ExprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitUnary" ):
                return visitor.visitUnary(self)
            else:
                return visitor.visitChildren(self)


    class AtomContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(RParser.ID, 0)
        def CHARACTER(self):
            return self.getToken(RParser.CHARACTER, 0)
        def HEX(self):
            return self.getToken(RParser.HEX, 0)
        def INT(self):
            return self.getToken(RParser.INT, 0)
        def FLOAT(self):
            return self.getToken(RParser.FLOAT, 0)
        def COMPLEX(self):
            return self.getToken(RParser.COMPLEX, 0)
        def NULL(self):
            return self.getToken(RParser.NULL, 0)
        def NA(self):
            return self.getToken(RParser.NA, 0)
        def Inf(self):
            return self.getToken(RParser.Inf, 0)
        def NaN(self):
            return self.getToken(RParser.NaN, 0)
        def TRUE(self):
            return self.getToken(RParser.TRUE, 0)
        def FALSE(self):
            return self.getToken(RParser.FALSE, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAtom" ):
                return visitor.visitAtom(self)
            else:
                return visitor.visitChildren(self)


    class CompareContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RParser.ExprContext
            super().__init__(parser)
            self.op = None # Token
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RParser.ExprContext)
            else:
                return self.getTypedRuleContext(RParser.ExprContext,i)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCompare" ):
                return visitor.visitCompare(self)
            else:
                return visitor.visitChildren(self)


    class AssignContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RParser.ExprContext
            super().__init__(parser)
            self.op = None # Token
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RParser.ExprContext)
            else:
                return self.getTypedRuleContext(RParser.ExprContext,i)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAssign" ):
                return visitor.visitAssign(self)
            else:
                return visitor.visitChildren(self)


    class FunctionCallContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(RParser.ExprContext,0)

        def sublist(self):
            return self.getTypedRuleContext(RParser.SublistContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFunctionCall" ):
                return visitor.visitFunctionCall(self)
            else:
                return visitor.visitChildren(self)


    class LogicAndContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RParser.ExprContext
            super().__init__(parser)
            self.op = None # Token
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RParser.ExprContext)
            else:
                return self.getTypedRuleContext(RParser.ExprContext,i)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLogicAnd" ):
                return visitor.visitLogicAnd(self)
            else:
                return visitor.visitChildren(self)


    class VectorContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RParser.ExprContext)
            else:
                return self.getTypedRuleContext(RParser.ExprContext,i)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVector" ):
                return visitor.visitVector(self)
            else:
                return visitor.visitChildren(self)


    class ParenthesesContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(RParser.ExprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParentheses" ):
                return visitor.visitParentheses(self)
            else:
                return visitor.visitChildren(self)


    class SeqContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def sublist(self):
            return self.getTypedRuleContext(RParser.SublistContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSeq" ):
                return visitor.visitSeq(self)
            else:
                return visitor.visitChildren(self)


    class PowerContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RParser.ExprContext
            super().__init__(parser)
            self.op = None # Token
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RParser.ExprContext)
            else:
                return self.getTypedRuleContext(RParser.ExprContext,i)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPower" ):
                return visitor.visitPower(self)
            else:
                return visitor.visitChildren(self)



    def expr(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = RParser.ExprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 2
        self.enterRecursionRule(localctx, 2, self.RULE_expr, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 56
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [RParser.T__1]:
                localctx = RParser.ParenthesesContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx

                self.state = 26
                self.match(RParser.T__1)
                self.state = 27
                self.expr(0)
                self.state = 28
                self.match(RParser.T__2)
                pass
            elif token in [RParser.SUB]:
                localctx = RParser.SignChangeContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx

                self.state = 30
                self.match(RParser.SUB)
                self.state = 31
                self.expr(25)
                pass
            elif token in [RParser.T__12]:
                localctx = RParser.UnaryContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 32
                localctx.op = self.match(RParser.T__12)
                self.state = 33
                self.expr(20)
                pass
            elif token in [RParser.T__17]:
                localctx = RParser.UnaryContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 34
                self.match(RParser.T__17)
                self.state = 35
                self.expr(17)
                pass
            elif token in [RParser.T__23]:
                localctx = RParser.SeqContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 36
                self.match(RParser.T__23)
                self.state = 37
                self.sublist()
                self.state = 38
                self.match(RParser.T__2)
                pass
            elif token in [RParser.T__24]:
                localctx = RParser.CompoundContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 40
                self.match(RParser.T__24)
                self.state = 41
                self.exprlist()
                self.state = 42
                self.match(RParser.T__25)
                pass
            elif token in [RParser.ID]:
                localctx = RParser.AtomContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 44
                self.match(RParser.ID)
                pass
            elif token in [RParser.CHARACTER]:
                localctx = RParser.AtomContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 45
                self.match(RParser.CHARACTER)
                pass
            elif token in [RParser.HEX]:
                localctx = RParser.AtomContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 46
                self.match(RParser.HEX)
                pass
            elif token in [RParser.INT]:
                localctx = RParser.AtomContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 47
                self.match(RParser.INT)
                pass
            elif token in [RParser.FLOAT]:
                localctx = RParser.AtomContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 48
                self.match(RParser.FLOAT)
                pass
            elif token in [RParser.COMPLEX]:
                localctx = RParser.AtomContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 49
                self.match(RParser.COMPLEX)
                pass
            elif token in [RParser.NULL]:
                localctx = RParser.AtomContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 50
                self.match(RParser.NULL)
                pass
            elif token in [RParser.NA]:
                localctx = RParser.AtomContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 51
                self.match(RParser.NA)
                pass
            elif token in [RParser.Inf]:
                localctx = RParser.AtomContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 52
                self.match(RParser.Inf)
                pass
            elif token in [RParser.NaN]:
                localctx = RParser.AtomContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 53
                self.match(RParser.NaN)
                pass
            elif token in [RParser.TRUE]:
                localctx = RParser.AtomContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 54
                self.match(RParser.TRUE)
                pass
            elif token in [RParser.FALSE]:
                localctx = RParser.AtomContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 55
                self.match(RParser.FALSE)
                pass
            else:
                raise NoViableAltException(self)

            self._ctx.stop = self._input.LT(-1)
            self.state = 89
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,5,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 87
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,4,self._ctx)
                    if la_ == 1:
                        localctx = RParser.PowerContext(self, RParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 58
                        if not self.precpred(self._ctx, 26):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 26)")
                        self.state = 59
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not(_la==RParser.T__3 or _la==RParser.T__4):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 60
                        self.expr(26)
                        pass

                    elif la_ == 2:
                        localctx = RParser.VectorContext(self, RParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 61
                        if not self.precpred(self._ctx, 24):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 24)")
                        self.state = 62
                        self.match(RParser.T__5)
                        self.state = 63
                        self.expr(25)
                        pass

                    elif la_ == 3:
                        localctx = RParser.MulDivContext(self, RParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 64
                        if not self.precpred(self._ctx, 23):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 23)")
                        self.state = 65
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not(_la==RParser.MUL or _la==RParser.DIV):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 66
                        self.expr(24)
                        pass

                    elif la_ == 4:
                        localctx = RParser.AddSubContext(self, RParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 67
                        if not self.precpred(self._ctx, 22):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 22)")
                        self.state = 68
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not(_la==RParser.ADD or _la==RParser.SUB):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 69
                        self.expr(23)
                        pass

                    elif la_ == 5:
                        localctx = RParser.CompareContext(self, RParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 70
                        if not self.precpred(self._ctx, 21):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 21)")
                        self.state = 71
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << RParser.T__6) | (1 << RParser.T__7) | (1 << RParser.T__8) | (1 << RParser.T__9) | (1 << RParser.T__10) | (1 << RParser.T__11))) != 0)):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 72
                        self.expr(22)
                        pass

                    elif la_ == 6:
                        localctx = RParser.LogicAndContext(self, RParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 73
                        if not self.precpred(self._ctx, 19):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 19)")
                        self.state = 74
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not(_la==RParser.T__13 or _la==RParser.T__14):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 75
                        self.expr(20)
                        pass

                    elif la_ == 7:
                        localctx = RParser.LogicOrContext(self, RParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 76
                        if not self.precpred(self._ctx, 18):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 18)")
                        self.state = 77
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not(_la==RParser.T__15 or _la==RParser.T__16):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 78
                        self.expr(19)
                        pass

                    elif la_ == 8:
                        localctx = RParser.AssignContext(self, RParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 79
                        if not self.precpred(self._ctx, 16):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 16)")
                        self.state = 80
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << RParser.T__18) | (1 << RParser.T__19) | (1 << RParser.T__20) | (1 << RParser.T__21) | (1 << RParser.T__22))) != 0)):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 81
                        self.expr(17)
                        pass

                    elif la_ == 9:
                        localctx = RParser.FunctionCallContext(self, RParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 82
                        if not self.precpred(self._ctx, 15):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 15)")
                        self.state = 83
                        self.match(RParser.T__1)
                        self.state = 84
                        self.sublist()
                        self.state = 85
                        self.match(RParser.T__2)
                        pass

             
                self.state = 91
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,5,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class ExprlistContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RParser.ExprContext)
            else:
                return self.getTypedRuleContext(RParser.ExprContext,i)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(RParser.NL)
            else:
                return self.getToken(RParser.NL, i)

        def getRuleIndex(self):
            return RParser.RULE_exprlist

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExprlist" ):
                return visitor.visitExprlist(self)
            else:
                return visitor.visitChildren(self)




    def exprlist(self):

        localctx = RParser.ExprlistContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_exprlist)
        self._la = 0 # Token type
        try:
            self.state = 103
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [RParser.T__1, RParser.T__12, RParser.T__17, RParser.T__23, RParser.T__24, RParser.HEX, RParser.INT, RParser.FLOAT, RParser.SUB, RParser.NA, RParser.Inf, RParser.NaN, RParser.NULL, RParser.TRUE, RParser.FALSE, RParser.COMPLEX, RParser.CHARACTER, RParser.ID]:
                self.enterOuterAlt(localctx, 1)
                self.state = 92
                self.expr(0)
                self.state = 99
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==RParser.T__0 or _la==RParser.NL:
                    self.state = 93
                    _la = self._input.LA(1)
                    if not(_la==RParser.T__0 or _la==RParser.NL):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()
                    self.state = 95
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << RParser.T__1) | (1 << RParser.T__12) | (1 << RParser.T__17) | (1 << RParser.T__23) | (1 << RParser.T__24) | (1 << RParser.HEX) | (1 << RParser.INT) | (1 << RParser.FLOAT) | (1 << RParser.SUB) | (1 << RParser.NA) | (1 << RParser.Inf) | (1 << RParser.NaN) | (1 << RParser.NULL) | (1 << RParser.TRUE) | (1 << RParser.FALSE) | (1 << RParser.COMPLEX) | (1 << RParser.CHARACTER) | (1 << RParser.ID))) != 0):
                        self.state = 94
                        self.expr(0)


                    self.state = 101
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                pass
            elif token in [RParser.T__25]:
                self.enterOuterAlt(localctx, 2)

                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SublistContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def sub(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RParser.SubContext)
            else:
                return self.getTypedRuleContext(RParser.SubContext,i)


        def getRuleIndex(self):
            return RParser.RULE_sublist

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSublist" ):
                return visitor.visitSublist(self)
            else:
                return visitor.visitChildren(self)




    def sublist(self):

        localctx = RParser.SublistContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_sublist)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 105
            self.sub()
            self.state = 110
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==RParser.T__26:
                self.state = 106
                self.match(RParser.T__26)
                self.state = 107
                self.sub()
                self.state = 112
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SubContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self):
            return self.getTypedRuleContext(RParser.ExprContext,0)


        def ID(self):
            return self.getToken(RParser.ID, 0)

        def CHARACTER(self):
            return self.getToken(RParser.CHARACTER, 0)

        def getRuleIndex(self):
            return RParser.RULE_sub

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSub" ):
                return visitor.visitSub(self)
            else:
                return visitor.visitChildren(self)




    def sub(self):

        localctx = RParser.SubContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_sub)
        try:
            self.state = 132
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,10,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 113
                self.expr(0)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 114
                self.match(RParser.ID)
                self.state = 115
                self.match(RParser.T__20)
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 116
                self.match(RParser.ID)
                self.state = 117
                self.match(RParser.T__20)
                self.state = 118
                self.expr(0)
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 119
                self.match(RParser.CHARACTER)
                self.state = 120
                self.match(RParser.T__20)
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 121
                self.match(RParser.CHARACTER)
                self.state = 122
                self.match(RParser.T__20)
                self.state = 123
                self.expr(0)
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 124
                self.match(RParser.NULL)
                self.state = 125
                self.match(RParser.T__20)
                pass

            elif la_ == 7:
                self.enterOuterAlt(localctx, 7)
                self.state = 126
                self.match(RParser.NULL)
                self.state = 127
                self.match(RParser.T__20)
                self.state = 128
                self.expr(0)
                pass

            elif la_ == 8:
                self.enterOuterAlt(localctx, 8)
                self.state = 129
                self.match(RParser.T__27)
                pass

            elif la_ == 9:
                self.enterOuterAlt(localctx, 9)
                self.state = 130
                self.match(RParser.T__28)
                pass

            elif la_ == 10:
                self.enterOuterAlt(localctx, 10)

                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[1] = self.expr_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expr_sempred(self, localctx:ExprContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 26)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 24)
         

            if predIndex == 2:
                return self.precpred(self._ctx, 23)
         

            if predIndex == 3:
                return self.precpred(self._ctx, 22)
         

            if predIndex == 4:
                return self.precpred(self._ctx, 21)
         

            if predIndex == 5:
                return self.precpred(self._ctx, 19)
         

            if predIndex == 6:
                return self.precpred(self._ctx, 18)
         

            if predIndex == 7:
                return self.precpred(self._ctx, 16)
         

            if predIndex == 8:
                return self.precpred(self._ctx, 15)
         




