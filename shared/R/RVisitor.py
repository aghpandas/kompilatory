# Generated from R.g4 by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .RParser import RParser
else:
    from RParser import RParser

# This class defines a complete generic visitor for a parse tree produced by RParser.

class RVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by RParser#prog.
    def visitProg(self, ctx:RParser.ProgContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RParser#LogicOr.
    def visitLogicOr(self, ctx:RParser.LogicOrContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RParser#MulDiv.
    def visitMulDiv(self, ctx:RParser.MulDivContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RParser#AddSub.
    def visitAddSub(self, ctx:RParser.AddSubContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RParser#Compound.
    def visitCompound(self, ctx:RParser.CompoundContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RParser#SignChange.
    def visitSignChange(self, ctx:RParser.SignChangeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RParser#Unary.
    def visitUnary(self, ctx:RParser.UnaryContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RParser#Atom.
    def visitAtom(self, ctx:RParser.AtomContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RParser#Compare.
    def visitCompare(self, ctx:RParser.CompareContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RParser#Assign.
    def visitAssign(self, ctx:RParser.AssignContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RParser#FunctionCall.
    def visitFunctionCall(self, ctx:RParser.FunctionCallContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RParser#LogicAnd.
    def visitLogicAnd(self, ctx:RParser.LogicAndContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RParser#Vector.
    def visitVector(self, ctx:RParser.VectorContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RParser#Parentheses.
    def visitParentheses(self, ctx:RParser.ParenthesesContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RParser#Seq.
    def visitSeq(self, ctx:RParser.SeqContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RParser#Power.
    def visitPower(self, ctx:RParser.PowerContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RParser#exprlist.
    def visitExprlist(self, ctx:RParser.ExprlistContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RParser#sublist.
    def visitSublist(self, ctx:RParser.SublistContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RParser#sub.
    def visitSub(self, ctx:RParser.SubContext):
        return self.visitChildren(ctx)



del RParser