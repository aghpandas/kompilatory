import sys
from antlr4 import *

from RLexer import RLexer
from RParser import RParser
from BaseRVisitor import BaseRVisitor
from InteractiveStream import InteractiveStream


def main(argv):
    if len(argv) > 1:
        text = FileStream(argv[1])
        lexer = RLexer(text)
        interpret(lexer)
    else:
        print(f"Welcome to the interactive console!\nYour command will be executed after you click Enter.\nTo finish type 'exit'. Have fun!\n")
        while True:
            line = bytes(input('> '), 'ascii')
            # line = sys.stdin.buffer.readline()
            if b'exit' in line:
                print('Bye!')
                break
            lexer = RLexer(InteractiveStream(line))
            interpret(lexer)


def interpret(lexer):
    stream = CommonTokenStream(lexer)
    parser = RParser(stream)
    tree = parser.prog()
    visitor = BaseRVisitor()

    try:
        visitor.visit(tree)
    except Exception as e:
        print(f"{e.__class__.__name__}: {e}")


if __name__ == '__main__':
    main(sys.argv)
